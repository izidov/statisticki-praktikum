hormon <- read.csv2("hormon.csv")
str(hormon)
attach(hormon)

table(spol)
prop.table(table(spol))
barplot(table(spol))

mean(somatZ,na.rm = TRUE)
somatZ[!is.na(somatZ)]
median(somatZ[!is.na(somatZ)])
mean(somatZ[!is.na(somatZ)])
sd((somatZ[!is.na(somatZ)]))
IQR(somatZ[!is.na(somatZ)])

fivenum(somatZ,na.rm = TRUE)
hist(somatZ)
boxplot(somatZ)

kava
spol
table(kava)
table(spol)
tab <- table(spol,alkohol)
prop.table(tab)
addmargins(tab)
addmargins(prop.table(tab))

prop.table(tab,1)
prop.table(tab,2)
addmargins(tab)
barplot(tab)
barplot(tab,beside = TRUE,legend=row.names(tab))
barplot(prop.table(tab,1),beside = TRUE,legend=row.names(tab))
