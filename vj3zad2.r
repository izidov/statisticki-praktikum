djelatnici <- read.csv2("djelatnici.csv")
str(djelatnici)
attach(djelatnici)
table(odjel)
prop.table(table(obrazovanje))
barplot(prop.table(table(obrazovanje)))
prop.table(table(obrazovanje,spol))
prop.table(table(odjel,spol),1)
prop.table(table(odjel,obrazovanje),2)
prop.table(table(odjel,obrazovanje))

barplot(prop.table(table(odjel,spol),1),beside = TRUE)
barplot(prop.table(table(spol,odjel),2),beside = TRUE)

barplot(prop.table(table(odjel,spol),1),beside = TRUE)
barplot(prop.table(table(spol,odjel),2),beside = TRUE)

table(odjel,spol,obrazovanje)

mean(placa_prije[spol=="M"])
mean(placa_prije[spol=="Z"])

median(placa_prije[spol=="M"])
median(placa_prije[spol=="Z"])

sd(placa_prije[spol=="M"])
sd(placa_prije[spol=="Z"])

summary(placa_prije[spol=="M"])
summary(placa_prije[spol=="Z"])

par(mfrow=c(1,2))
hist(placa_prije[spol=="M"])
hist(placa_prije[spol=="Z"])
dev.off()

boxplot(placa_prije[spol=="Z"],placa_prije[spol=="M"])
boxplot(placa_prije~spol)

tapply(placa_prije, spol, mean)
tapply(placa_prije, list(spol,obrazovanje), mean)

tapply(placa_prije, odjel, mean)
tapply(placa_poslije, odjel, mean)
tapply(placa_poslije, list(odjel,spol), median)
boxplot(placa_prije~obrazovanje+spol)

tapply(placa_prije, obrazovanje, mean)
tapply(placa_prije, obrazovanje, median)

boxplot(placa_prije~obrazovanje)
obrazovanje2 <- ordered(obrazovanje,levels=c("SSS","VŠSS","VSS"))
boxplot(placa_prije~obrazovanje2)

tapply(placa_prije, odjel, sd)

tapply(placa_poslije, odjel, mean)-tapply(placa_prije, odjel, mean)


plot(placa_prije,placa_poslije)
plot(placa_poslije~placa_prije)
